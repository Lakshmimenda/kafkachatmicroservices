# KafkaChatMicroservices : Please follow the below before starting the application: 
1. Place the server.properties & zookeeper.properties files in the Kafka/config folder 
2. Start the zookeeper by executing the below command :
.\bin\windows\zookeeper-server-start.bat .\config\zookeeper.properties
3. Start the Kafka Server by executing the below command : 
.\bin\windows\kafka-server-start.bat .\config\server.properties

To Test the User to Admin Chat, please run the mircoservices in the below order : 
1. Run the KafkaAdminMicroservice
2. Run the KafkaUserMicroservice 

the recieve endpoints are given to show the entire chat. the person can view the latest message from the recieve endpoints and reply back using the produce endpoints.

To test the user to user chat, please run the mircoservices in the below order: 
1. Run the KafkaAdminMicroservice
2. Run the KafkaUserMicroservice 
3. Run the KafkaUserMicroservice as another application with port as 8082 in application properties

Note : Utilzing Kafka 2.8.0/kafka_2.13-2.8.0.tgz

Console Tests from the Kafka servers can be done using  : 
1. To view list of Kafka-topics
.\bin\windows\kafka-topics.bat --list --bootstrap-server localhost:9092
2. To create a new topic
.\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic testtopic  
3. To delete existing topic 
.\bin\windows\kafka-topics.bat --delete  --zookeeper localhost:2181 --topic testtopic
4. To start build in console producer:
.\bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic testtopic
5. To start build in console Consumer:
.\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic testtopic --from-beginning
6. To Stop Kafka Server
.\bin\windows\kafka-server-stop.bat .\config\server.properties
 
