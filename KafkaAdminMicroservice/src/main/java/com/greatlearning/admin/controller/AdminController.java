package com.greatlearning.admin.controller;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

	// Admin Controller allowing the admin to send and recieve messages from users.
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	// Defining temp List to display the data been interchanged between user and admin.
	List<String> messages = new ArrayList<String>();
	List<String> messagesOffline = new ArrayList<String>();

	// Kafka Admin Listener
	@KafkaListener(topics = { "admin" })
	public void getTopics(String message) {
		System.out.println("Kafka event consumed is: " + message);
		messages.add(message);
	}

	// Allows the admin to send message to the user
	@PostMapping("/produce")
	public String sendMessage(@RequestBody String message) {
	 kafkaTemplate.send("admin",message);
		return "Message Sent";
	}

	// displays the chat between user and admin
	@GetMapping("/recieve")
	public List<String> getMessages(){
		return messages;
	}
	
	// Kafka Offline User listerner consuming messages from adminOffline topic
	@KafkaListener(topics = { "adminOffline" })
	public void getTopicsOffline(String message) {
		System.out.println("Kafka event consumed is: " + message);
		messagesOffline.add(message);
	}

	// Allows the admin to send messages to offline users
	@PostMapping("/produceOfflineMessages")
	public String sendMessageOffline(@RequestBody String message) {
	 kafkaTemplate.send("adminOffline",message);
		return "Message Sent";
	}

	// displays the chat between admin and offline user
	@GetMapping("/recieveOfflineMessages")
	public List<String> getMessagesOffline(){
		return messagesOffline;
	}
}
