package com.greatlearning.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaUserMicroservice {

	// Main Method
	public static void main(String[] args) {
		SpringApplication.run(KafkaUserMicroservice.class, args);
	}

}
